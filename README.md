# README #

This repository contains files associated with morphological and evolutionary analyses of Australian blind snakes and kin. 

## Data

- Linear measurements are raw measurements using calipers on preserved specimens from natural history collections. 

- Photograph of snake heads (dorsal, lateral, and ventral views). Images were digitised with landmarks and semilandmarks and used for 2D Geometric morphometric analyses with *GeoMorph*. 

## Scripts


## Required softwares
- *R* and several packages for analyses (see script)
- tpsDig / tpsUtil / tpsRelw for managing and digitising landmarks on photographs.
