Spags and snags: the evolution of Australia's blindsnakes 

Abstract: 

TLDR; I will tell you about Australia's most diverse snake genus, their relationships with one another, how they got to where they are, and what their morphological and ecological variation can tell us about their evolution and their future.

Australia harbours immense biodiversity, in part thanks to the variety of biomes from tropical rainforest to arid desert, and its past geological connections to Asia and New Guinea.
Among terrestrial vertebrates, lizards and snakes represent the most species-rich group with over 1000 species described, the highest in the world for any one particular country.
Some of the most iconic reptiles like elapid snakes, pythons, sphenomorphine skinks, dragons, and monitor lizards showed dramatic ecological and morphological diversity. However, a few other clades have also undergone extensive lineage diversification but with more subtle ecological and/or morphological divergence, such as blindsnakes. Despite being the most species rich genus of snakes in Australia, blindsnakes are often overlooked, partially because they all live underground and we know so little about their natural history. In this seminar, I will first give blindsnakes the introduction they deserve then tell present the most comprehensive species tree to date for the Australian genus. I will then present what we can estimate about their historical biogeography and discuss how past environmental changes in Australia may have influenced their diversification. Lastly, I will talk about what the morphological and ecological differences we observe today can tell us about how they evolved and what their future might look like. 
