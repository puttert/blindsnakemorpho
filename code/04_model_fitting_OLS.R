# 04_model_fitting_OLS.R
# Sarin Tiatragul 
# March 2023

# Fitting continuos trait using ordinary least squares 

# libraries ---------------------------------------------------------------
# Packages for analyses
library(mvMORPH); library(geiger)

# For plotting stuff
library(viridis); library(R.utils)
library(RColorBrewer)
'%notin%' <- Negate('%in%')

# data --------------------------------------------------------------------

load("data/script_generated_data/conventional_pca.rda")

# Load two D array of y symmetric component of dorsal head shape
load('data/script_generated_data/dorsal_head_shape.rda')

# Load data frame of summarised traits
blindsnake_data_full

anilios_df <- blindsnake_data_full[which(str_detect(blindsnake_data_full$species, pattern = "Anilios")),]

anilios_ols_df <- anilios_df[anilios_df$species != "Anilios_erycinus",]

# Environmental predictors
soil.bulk <- anilios_ols_df$mean_bulk; names(soil.bulk) <- rownames(anilios_ols_df)
temp_m <- anilios_ols_df$temp_mean; names(temp_m) <- rownames(anilios_ols_df)
arid_m <- anilios_ols_df$ARID; names(arid_m) <- rownames(anilios_ols_df)

anilios_ols_df$log_maxsvl <- log(anilios_ols_df$max_svl)


# Body size alone ---------------------------------------------------------
ols_temp_svl <- lm(log_maxsvl ~ temp_mean, data=anilios_ols_df)
summary(ols_temp_svl)
plot(log_maxsvl ~ temp_mean, data=anilios_ols_df)

ols_soil_svl <- lm(log_maxsvl ~ mean_bulk, data=anilios_ols_df)
summary(ols_soil_svl)

ols_arid_svl <- lm(log_maxsvl ~ ARID, data=anilios_ols_df)
summary(ols_arid_svl)
plot(log_maxsvl ~ ARID, data=anilios_ols_df)

# Interpretation
#OLS (LM) result is the same with phylo lm

log(0.97044)
exp(-0.03)


# Multivariate trait ------------------------------------------------------

# Add the predictors to the previous 'data' list
Y_body <- as.matrix(anilios_ols_df[, c("sh_mbd", "sh_mtw", "sh_hwe", "sh_hda", "sh_tl")])

body_sh_traits <- list(Y_body=Y_body, soil = soil.bulk, temp = temp_m, arid = arid_m)

# Head ~ soil compactness? 
ols_body_soil <- mvols(Y_body ~ soil, data=body_sh_traits)
aov_soil_bd <- manova.gls(ols_body_soil, nperm=999, test="Pillai", type = "II")
aov_soil_bd

# Head ~ temperature
ols_body_temp <- mvols(Y_body ~ temp, data=body_sh_traits)
aov_temp_bd <- manova.gls(ols_body_temp, nperm=999, test="Pillai", type = "II")
aov_temp_bd

# Head ~ aridity
ols_body_arid <- mvols(Y_body ~ arid, data=body_sh_traits)
aov_arid_bd <- manova.gls(ols_body_arid, nperm=999, test="Pillai", type = "II")
aov_arid_bd

ols_body_mult <- mvols(Y_body ~ soil + temp + arid, data=body_sh_traits)
aov_mult_bd <- manova.gls(ols_body_mult, nperm=999, test="Pillai", type = "II")
aov_mult_bd



plot(anilios_ols_df$sh_mbd ~ anilios_ols_df$temp_mean)
plot(anilios_ols_df$sh_mtw ~ anilios_ols_df$temp_mean)
plot(anilios_ols_df$sh_hwe ~ anilios_ols_df$temp_mean)
