# 04_model_fitting_raw.R
# Sarin Tiatragul 
# March 2023
# Fitting continuos trait evolution models and testing for
# phylogenetic signal in various traits using raw values not residuals of head shape

# libraries ---------------------------------------------------------------
# Packages for analyses
library(phytools); library(ape); library(mvMORPH); library(geiger)
library(convevol); library(phylolm); library(dplyr)

# For plotting stuff
library(viridis); library(R.utils)
library(RColorBrewer)
'%notin%' <- Negate('%in%')
source('code/utility/func_calc_ses.R')

##
#### MAIN QUESTIONS
## 
# * Which evolutionary model best fit the data?
# * Is there phylogenetic signal?
# * What is the best predictor of body shape variation in blind snakes? 
# * Are there convergence to a particular body trait in certain environments
# * Does the rate of trait evolution vary by biome
# * How much variation can be explained by phylogentic relatedness alone

# data --------------------------------------------------------------------

# Load full tree, subsetted tree, and subset 
load('data/script_generated_data/subset_mcmctree_shape.rda')
anilios_tree <- ape::drop.tip(sub_phy, tip = c("Ramphotyphlops_multilineatus","Acutotyphlops_subocularis"))
# anilios_tree <- ape::drop.tip(blindsnake_tree, tip = c("Ramphotyphlops_multilineatus","Acutotyphlops_subocularis"))

# Load two D array of y symmetric component of dorsal head shape
# load('data/script_generated_data/dorsal_head_shape.rda')
load('data/script_generated_data/dorsal_head_shape_raw.rda')

# Load data frame of summarised traits
anilios_data <- read.csv(file = 'data/script_generated_data/anilios_summary_data.csv', row.names = 1)


# Model fitting -----------------------------------------------------------

###
# * Which evolutionary model best fit the data?
###

#### Univariate

fit1 <- phylolm(log(max_svl) ~ 1, data = anilios_data, phy = anilios_tree, model = "BM", measurement_error = TRUE, boot=100)
fit2 <- phylolm(log(max_svl) ~ 1, data = anilios_data, phy = anilios_tree, model = "OUrandomRoot", measurement_error = TRUE, boot=100)
fit3 <- phylolm(log(max_svl) ~ 1, data = anilios_data, phy = anilios_tree, model = "OUfixedRoot", measurement_error = TRUE, boot=100)
fit4 <- phylolm(log(max_svl) ~ 1, data = anilios_data, phy = anilios_tree, model = "EB", measurement_error = TRUE, lower.bound = -10, upper.bound = 10, boot=100)
fit5 <- phylolm(log(max_svl) ~ 1, data = anilios_data, phy = anilios_tree, model = "lambda")

AIC(fit1); AIC(fit2); AIC(fit3); AIC(fit4); AIC(fit5) # EB is the best model
# fit1$logLik; fit2$logLik; fit3$logLik; fit4$logLik; fit5$logLik # EB is the best model
# fit1$sigma2; fit2$sigma2; fit3$sigma2; fit4$sigma2; fit5$sigma2 # EB is the best model
# fit1$sigma2_error; fit2$sigma2_error; fit3$sigma2_error; fit4$sigma2_error; fit5$sigma2_error # EB is the best model

# Results tabulated in excel sheet 

#### Using mvMORPH package we fit multivariate data set for body traits and head shape
#### Multivariate body traits include log shape ratios of mbd, mtw, hwe, hdea, tl 

Y_body <- as.matrix(anilios_data[, c("sh_mbd", "sh_mtw", "sh_hwe", "sh_hda", "sh_tl")])
body_sh_traits <- list(Y_body=Y_body) # prepared as list for mvMORPH

# Fit the 'BM', 'OU', and 'EB' models to 'Y' using mvMORPH::mvgls
# Using ridge quadratic null penalised likelihood LOOCV (recommended for low and high dimensional multivariate traits from in Clavel et al. 2019)
fit_bsh_bm <- mvgls(Y_body~1, data=body_sh_traits, anilios_tree, "BM", penalty="RidgeArch", target = "unitVariance", method="PL-LOOCV", error = TRUE)
fit_bsh_ou <- mvgls(Y_body~1, data=body_sh_traits, anilios_tree, "OU", penalty="RidgeArch", target = "unitVariance", method="PL-LOOCV", error = TRUE)
fit_bsh_eb <- mvgls(Y_body~1, data=body_sh_traits, anilios_tree, "EB", penalty="RidgeArch", target = "unitVariance", method="PL-LOOCV", error = TRUE)
fit_bsh_lambda <- mvgls(Y_body~1, data=body_sh_traits, anilios_tree, "lambda", penalty="RidgeArch", target = "unitVariance", method="PL-LOOCV")

# Combine model fit in list
bsh_model_fit <- list(fit_bsh_bm, fit_bsh_ou, fit_bsh_eb, fit_bsh_lambda)

# Data table to compare
fit_bsh_GIC <- data.frame(models = c("BM", "OU", "EB", "Lambda"), GIC = NA)
for (i in 1:length(bsh_model_fit)) {
  fit_bsh_GIC$GIC[i] <- GIC(bsh_model_fit[[i]])$GIC
}

# GIC Table for body shape
fit_bsh_GIC %>% dplyr::arrange(GIC)

summary(fit_bsh_bm)
summary(fit_bsh_lambda)
summary(fit_bsh_ou)
summary(fit_bsh_eb)

#### Multivariate head traits from symmetric component
# Array of mean dorsal landmarks from gpa and symmetric analyses from dorsal_headshape.rda

rownames(sp_means_dorsal) <- gsub(x = rownames(sp_means_dorsal), pattern = "^", replacement = "Anilios_")
means_2d_dorsal <- sp_means_dorsal[rownames(sp_means_dorsal) %in% anilios_tree$tip.label,]
means_2d_dorsal <- means_2d_dorsal[match(anilios_tree$tip.label, rownames(means_2d_dorsal)), ]

# Turn head landmark traits 
Y_head <- as.matrix(means_2d_dorsal[-c(1,2)])
head_traits <- list(Y_head=Y_head, log_c_size = log(means_2d_dorsal$c_size))

## Test for shape-size allometry across species 
# We include log_c_size here to account for allometric effects. The residuals is size-free shape
fit_head_bm <- mvgls(Y_head~log_c_size, data=head_traits, anilios_tree, "BM", penalty="RidgeArch", target = "unitVariance", method="PL-LOOCV", error = TRUE)
fit_head_ou <- mvgls(Y_head~log_c_size, data=head_traits, anilios_tree, "OU", penalty="RidgeArch", target = "unitVariance", method="PL-LOOCV", error = TRUE)
fit_head_eb <- mvgls(Y_head~log_c_size, data=head_traits, anilios_tree, "EB", penalty="RidgeArch", target = "unitVariance", method="PL-LOOCV", error = TRUE)
fit_head_lambda <- mvgls(Y_head~log_c_size, data=head_traits, anilios_tree, "lambda", penalty="RidgeArch", target = "unitVariance", method="PL-LOOCV")

# Combine model fit in list
head_model_fit <- list(fit_head_bm, fit_head_ou, fit_head_eb, fit_head_lambda)

# Data table to compare
fit_head_GIC <- data.frame(models = c("BM", "OU", "EB", "Lambda"), GIC = NA)
for (i in 1:length(head_model_fit)) {
  fit_head_GIC$GIC[i] <- GIC(head_model_fit[[i]])$GIC
}

# EB has the lowest GIC score for head traits

# GIC Table for body shape
fit_head_GIC %>% dplyr::arrange(GIC)

summary(fit_head_bm)
summary(fit_head_ou)
summary(fit_head_eb)
summary(fit_head_lambda)
# manova.gls(fit_head_eb, nperm=999, test="Pillai", type = "II")
# No correlation between shape and size. But will use this fit obj for phylo PCA for size-free shape





###
### * Is there phylogenetic signal
###

summary(fit_bsh_lambda)
summary(fit_head_lambda)

# Phylogenetic signal in traits, closely related species tend to occupy similar morphospace
## Interpretation: Closely related species tend to have similar traits in both body shape and head

# Estimate PCA for traits -------------------------------------------------
## Following approach in Garcia-Porta et al 2022. We calculate PCA based on the variance covariance matrix 
## form the best evolutionary models

# dev.off()
body_pca_results <- mvgls.pca(fit_bsh_lambda, plot=T) 
head_pca_results <- mvgls.pca(fit_head_eb, plot=T)

rownames(body_pca_results$vectors) <- colnames(fit_bsh_lambda$coefficients)
rownames(body_pca_results$vectors)

body_pca_results$contrib <- round(body_pca_results$values / sum(body_pca_results$values) * 100, 1)
head_pca_results$contrib <- round(head_pca_results$values / sum(head_pca_results$values) * 100, 1)

# biplot(x=body_pca_results$scores[,1:2], y=body_pca_results$vectors[,1:2])

# body_pca_results

## Visualise in phoylomorphospace with PCA (not phy PCA)
load('data/script_generated_data/phylomorph_tip_colours.rda')

# pdf(file = 'output/mvmorph_PCA_output.pdf', width = 11, height = 8.5)

# pdf(file = 'output/combo_pca_phypca.pdf', width = 6.61417, height = 5)
par(mfrow=c(2,2))
# xlim = range(body_pca_df$PC1), ylim = range(body_pca_df$PC2),
body_pca_mat <- body_pca_results$scores[,1:2]
body_pca_mat[,1] <- body_pca_mat[,1]*-1

# Switch the PC vectors around (so that PC has all positive loadings)
phylomorphospace(anilios_tree, body_pca_mat, 
                 label = "horizontal", fsize = 1, ftype = 'i', bty='l',
                 xlab = paste("Body PC1 (", body_pca_results$contrib[1], "%)", sep=""),
                 ylab = paste("Body PC2 (", body_pca_results$contrib[2], "%)", sep=""), 
                 xlim = c(-2,2), ylim = c(-0.6, 0.6),
                 # xlim = range(-body_pca_mat[,1]), ylim = range(body_pca_mat[,2]),
                 pch = 20, cex = 0, node.size = c(0, 1.4),
                 control = list(col.node = soil_tip_cols))
# Include legend
legend_image <- as.raster(matrix(rev(my_palette)), ncol=1)
text(x=-1.5, y = seq(0, 0.4,l=5), labels = round(seq(min(anilios_data$mean_bulk, na.rm = T), max(anilios_data$mean_bulk, na.rm = T),l=5), 2))
rasterImage(legend_image, -1.5, 0, -1.25, 0.4)

head_pca_mat <- head_pca_results$scores[,1:2]*-1

# Plot head shape
phylomorphospace(anilios_tree, head_pca_mat, 
                 label = "horizontal", fsize = 1, ftype = 'i', bty='l',
                 xlab = paste("Head PC1 (", head_pca_results$contrib[1], "%)", sep=""),
                 ylab = paste("Head PC2 (", head_pca_results$contrib[2], "%)", sep=""), 
                 xlim = c(-0.2,0.2), ylim = c(-0.11, 0.06),
                 # xlim = range(head_pca_mat[,1]), ylim = range(head_pca_mat[,2])
                 xlim = c(-0.2, 0.2), ylim = c(-0.2, 0.2),
                 pch = 20, cex = 0, node.size = c(0, 1.4),
                 control = list(col.node = soil_tip_cols))

## Add conventional pca
load('data/script_generated_data/conventional_pca.rda')

# Phylomophospace of body
phylomorphospace(blindsnake_tree, blindsnake_data[, c("body_PC1", "body_PC2")], 
                 label = "horizontal", fsize = 1, ftype = 'i', bty='l',
                 xlab = "Body PC1 (80.5%)", ylab = "Body PC2 (12.2%)",
                 pch = 20, cex = 0, node.size = c(0, 1.4),
                 # xlim = c(-6,6), ylim = c(-2.75, 2.5),
                 xlim = range(body_pca_df$PC1), ylim = range(body_pca_df$PC2),
                 control = list(col.node = tip_cols_cont))
points(y = not_in_phy_df$body_PC2, x = not_in_phy_df$body_PC1, pch = 19, 
       col = not_in_phy_tip_colours, cex = 1.4)
text(y = not_in_phy_df$body_PC2 - 0.03, x = not_in_phy_df$body_PC1,
     labels = rownames(not_in_phy_df), cex = 0.8)
# Include legend (need to post process in illustrator)
legend_image <- as.raster(matrix(rev(my_palette)), ncol=1)
text(x=-3.8, y = seq(0,0.4,l=5), 
     labels = round(seq(min(blindsnake_data$mean_bulk, na.rm = T), 
                        max(blindsnake_data$mean_bulk, na.rm = T),l=5), 2))
rasterImage(legend_image, -4.2, 0, -4, 0.4)

# Plot head PC1 and PC2
phylomorphospace(blindsnake_tree, blindsnake_data[, c("mean_PC1", "mean_PC2")], 
                 label = "horizontal", fsize = 1, ftype = 'i', bty='l',
                 xlab = "Head PC1 (72.0%)", ylab = "Head PC2 (16.6%)",
                 pch = 20, cex = 0, node.size = c(0, 1.4),
                 # xlim = c(-0.2019596, 0.2), ylim = range(head_pca_df$head_PC2),
                 xlim = c(-0.2, 0.2), ylim = c(-0.15, 0.13),
                 # xlim = range(-head_pca_df$head_PC1), ylim = range(head_pca_df$head_PC2),
                 # ylim = c(range(blindsnake_data$mean_PC2)),
                 control = list(col.node = tip_cols_cont))
points(y = not_in_phy_df$mean_PC2, x = not_in_phy_df$mean_PC1, pch = 19, 
       col = not_in_phy_tip_colours, cex = 1.4)
text(y = not_in_phy_df$mean_PC2 - 0.005, x = not_in_phy_df$mean_PC1,
     labels = rownames(not_in_phy_df), cex = 0.8)

dev.off()


###
### * What is the best predictor of body shape variation in blind snakes? 
###
## With mvMORPH we can fit multivariate traits instead of PC scores. 

# Env predictors including soil compactness, percentage sand, mean temp, and mean prec
soil.bulk <- anilios_data$mean_bulk; names(soil.bulk) <- rownames(anilios_data)
temp_m <- anilios_data$temp_mean; names(temp_m) <- rownames(anilios_data)
arid_m <- anilios_data$ARID; names(arid_m) <- rownames(anilios_data)
arid_c <- anilios_data$arid_cat; names(arid_c) <- rownames(anilios_data)
# sand.pc <- anilios_data$mean_sand_per; names(sand.pc) <- rownames(anilios_data)
# prec_m <- anilios_data$prec_mean; names(prec_m) <- rownames(anilios_data)
# biome <- anilios_data$biome; names(biome) <- rownames(anilios_data)

## Fit univariate maximum svl
fit_max_svl_1 <- phylolm(log(max_svl) ~ mean_bulk + temp_mean + ARID, data = anilios_data, phy = anilios_tree, model = "lambda", boot = 100)
fit_max_svl_1eb <- phylolm(log(max_svl) ~ mean_bulk + temp_mean + ARID, data = anilios_data, phy = anilios_tree, model = "EB", measurement_error = TRUE, boot = 100)

summary(fit_max_svl_1eb)

cor(anilios_data$mean_bulk, anilios_data$ARID, method = "pearson")

fit_max_svl_2 <- phylolm(log(max_svl) ~ temp_mean, data = anilios_data, phy = anilios_tree, model = "lambda", boot = 100)
fit_max_svl_3 <- phylolm(log(max_svl) ~ mean_bulk, data = anilios_data, phy = anilios_tree, model = "lambda", boot = 100)
fit_max_svl_4 <- phylolm(log(max_svl) ~ ARID, data = anilios_data, phy = anilios_tree, model = "lambda", boot = 100)
fit_max_svl_5 <- phylolm(log(max_svl) ~ mean_bulk + temp_mean + ARID, data = anilios_data, phy = anilios_tree, model = "lambda", boot = 100)

summary(fit_max_svl_2)
summary(fit_max_svl_3)
summary(fit_max_svl_4)
summary(fit_max_svl_5)

par(mfrow=c(2,3))
plot(log(max_svl) ~ temp_mean, data = anilios_data)
plot(log(max_svl) ~ mean_bulk, data = anilios_data)
plot(log(max_svl) ~ ARID, data = anilios_data)


## INTERPRETATION
# Weakly negative correlation between temperature and max body size, 
# No effect of mean_bulk density or aridity on max log svl

###
### MULTIVARIATE MODEL FIT
###

# Add the predictors to the previous 'data' list
body_sh_traits_env <- list(Y_body=Y_body, soil = soil.bulk, temp = temp_m, arid = arid_m,
                           PCs = body_pca_results$scores)

# Difference body shape between arid and non arid species? 
## We use Pagel's lambda because this seen as a mixed model [@clavelReliable2020]

## Fit model individually

# Body ~ soil compactness? 
fit_soil <- mvgls(Y_body ~ soil, data=body_sh_traits_env, anilios_tree, model="lambda")
aov_soil_bd <- manova.gls(fit_soil, nperm=999, test="Pillai", type = "II")

# Body ~ temperature
fit_temp <- mvgls(Y_body ~ temp, data=body_sh_traits_env, anilios_tree, model="lambda")
aov_temp_bd <- manova.gls(fit_temp, nperm=999, test="Pillai", type = "II")

# Body ~ aridity
fit_arid <- mvgls(Y_body ~ arid, data=body_sh_traits_env, anilios_tree, model="lambda")
aov_arid_bd <- manova.gls(fit_arid, nperm=999, test="Pillai", type = "II")

## Fitted together
# Body ~ multiple variables
fit_all <- mvgls(Y_body ~ soil + temp + arid, data=body_sh_traits_env, anilios_tree, model="lambda")
# fit_allou <- mvgls(Y_body ~ soil + temp + biome, data=body_sh_traits_env, anilios_tree, model="OU", error = TRUE)
summary(fit_all)
aov_mult_bd <- manova.gls(fit_all, nperm=999, test="Pillai", type = "II")
ses.calc(aov_mult_bd)

### INTERPRETATION FOR FIT MULTIVARIATE BODY
#### Body Shape varies in a correlated way with both temperature and soil compactness, 
#### however, because the two are correlated geographically we cannot determine their relative importance from MANCOVA.

# PLOT SUPPLEMENTARY FIGURE BODY TRAITS AGAINST PREDICTORS
par(mfrow = c(5,3))
plot(arid_m, body_sh_traits_env$Y_body[,1])
plot(soil.bulk, body_sh_traits_env$Y_body[,1])
plot(temp_m, body_sh_traits_env$Y_body[,1])
plot(arid_m, body_sh_traits_env$Y_body[,2])
plot(soil.bulk, body_sh_traits_env$Y_body[,2])
plot(temp_m, body_sh_traits_env$Y_body[,2])
plot(arid_m, body_sh_traits_env$Y_body[,3])
plot(soil.bulk, body_sh_traits_env$Y_body[,3])
plot(temp_m, body_sh_traits_env$Y_body[,3])
plot(arid_m, body_sh_traits_env$Y_body[,4])
plot(soil.bulk, body_sh_traits_env$Y_body[,4])
plot(temp_m, body_sh_traits_env$Y_body[,4])
plot(arid_m, body_sh_traits_env$Y_body[,5])
plot(soil.bulk, body_sh_traits_env$Y_body[,5])
plot(temp_m, body_sh_traits_env$Y_body[,5])


par(mfrow=c(2,3))
plot(body_pca_results$scores[,1] ~ anilios_data$mean_bulk, bty ="n", pch=20, cex = 1.4,
     xlab = "Soil density", ylab = "Body PC1")
text(body_pca_results$scores[,1] ~ anilios_data$mean_bulk, labels = names(body_pca_results$scores[,1]))
plot(body_pca_results$scores[,1] ~ anilios_data$temp_mean, bty ="n", pch=20, cex = 1.4,
     xlab = "Temperature", ylab = "Body PC1")
text(body_pca_results$scores[,1] ~ anilios_data$temp_mean, labels = names(body_pca_results$scores[,1]))
plot(body_pca_results$scores[,1] ~ anilios_data$ARID, bty ="n", pch=20, cex = 1.4,
     xlab = "Temperature", ylab = "Body PC1")
text(body_pca_results$scores[,1] ~ anilios_data$ARID, labels = names(body_pca_results$scores[,1]))

plot(body_pca_results$scores[,2] ~ anilios_data$mean_bulk, bty ="n", pch=20, cex = 1.4,
     xlab = "Soil density", ylab = "Body PC2")
text(body_pca_results$scores[,2] ~ anilios_data$mean_bulk, labels = names(body_pca_results$scores[,1]))
plot(body_pca_results$scores[,2] ~ anilios_data$temp_mean, bty ="n", pch=20, cex = 1.4,
     xlab = "Temperature", ylab = "Body PC2")
text(body_pca_results$scores[,2] ~ anilios_data$temp_mean, labels = names(body_pca_results$scores[,1]))
plot(body_pca_results$scores[,2] ~ anilios_data$ARID, bty ="n", pch=20, cex = 1.4,
     xlab = "Temperature", ylab = "Body PC2")
text(body_pca_results$scores[,1] ~ anilios_data$ARID, labels = names(body_pca_results$scores[,1]))

###
### Fitting regression models with head traits
###

# In the first approach we regress head shape against env variables + log centroid size
head_traits_env <- list(Y_head = as.matrix(means_2d_dorsal[-c(1,2)]),
                        res_head = fit_head_lambda$residuals,
                        soil = soil.bulk, temp = temp_m, 
                        arid = arid_m, biome = arid_c,
                        PCs = head_pca_results$scores[,1:4],
                        log_c_size = log(means_2d_dorsal$c_size))

# Head ~ soil compactness? 
fit_head_soil <- mvgls(Y_head ~ soil + log_c_size + soil*log_c_size, data=head_traits_env, anilios_tree, model="lambda")
summary(fit_head_soil)
aov_soil_hd <- manova.gls(fit_head_soil, nperm=999, test="Pillai", type = "II")

# Head ~ temperature
fit_head_temp <- mvgls(Y_head ~ temp + log_c_size + temp*log_c_size, data=head_traits_env, anilios_tree, model="lambda")
# fit_head_tempeb <- mvgls(PCs ~ temp, data=head_traits_env, anilios_tree, model="OU", error = TRUE)
summary(fit_head_temp)
aov_temp_hd <- manova.gls(fit_head_temp, nperm=999, test="Pillai", type = "II")

# Head ~ aridity
fit_head_arid <- mvgls(Y_head ~ arid_m + log_c_size + arid_m*log_c_size, data=head_traits_env, anilios_tree, model="lambda")
summary(fit_head_arid)
aov_arid_hd <- manova.gls(fit_head_arid, nperm=999, test="Pillai", type = "II")

# # Head ~ multiple variables
# fit_head_all <- mvgls(Y_head ~ soil + temp + arid_m + log_c_size + log_c_size*soil*temp*arid_m, data=head_traits_env, anilios_tree, model="lambda")
# # fit_head_alleb <- mvgls(Y_head ~ soil + temp + biome, data=head_traits_env, anilios_tree, model="EB", error = TRUE)
# summary(fit_head_all)
# aov_mult_hd <- manova.gls(fit_head_all, nperm=999, test="Pillai", type = "II")
# 
# fit_head_all2 <- mvgls(Y_head ~ soil + temp + arid_m + log_c_size, data=head_traits_env, anilios_tree, model="lambda")
# aov_mult_hd2 <- manova.gls(fit_head_all2, nperm=999, test="Pillai", type = "II")

fit_head_all3 <- mvgls(Y_head ~ soil + temp + arid_m + log_c_size + soil*temp*arid_m, data=head_traits_env, anilios_tree, model="lambda")
aov_mult_hd3 <- manova.gls(fit_head_all3, nperm=999, test="Pillai", type = "II")

# Print results from MANOVA and MANCOVA
aov_soil_hd; aov_arid_hd; aov_temp_hd; aov_mult_hd3

fit_head_lambda$residuals

# Add the predictors to the previous 'data' list
head_traits_env <- list(Y_head = as.matrix(means_2d_dorsal[-c(1,2)]),
                        res_head = fit_head_lambda$residuals,
                        soil = soil.bulk, temp = temp_m, 
                        arid = arid_m, biome = arid_c,
                        PCs = head_pca_results$scores[,1:4],
                        log_c_size = log(means_2d_dorsal$c_size))


### INTERPRETATION FOR HEAD SHAPE REGRESSIONS
#### Variation in head shape is not explained by any of the predictors. 
#### Head shape appears to correlate with soil but this is not statistically significant. 

## Save data set used for these
save(head_traits_env, body_sh_traits_env, file = 'data/script_generated_data/05_env_traits_raw.rda')
save.image(file = "data/script_generated_data/mvmorph_fit_models_raw.RData")

# Load here instead of re-running the model
load("data/script_generated_data/mvmorph_fit_models_raw.RData")

## Tabulate models

# Max SVL
phylolm_tab <- rbind(as.data.frame(summary(fit_max_svl_2)["coefficients"])[-1,], as.data.frame(summary(fit_max_svl_3)["coefficients"])[-1,], 
                     as.data.frame(summary(fit_max_svl_4)["coefficients"])[-1,], as.data.frame(summary(fit_max_svl_5)["coefficients"])[-1,])

phylolm_tab$lambda <- c(fit_max_svl_2$optpar, fit_max_svl_3$optpar, fit_max_svl_4$optpar, rep(fit_max_svl_5$optpar, 3))
phylolm_tab$aic <- c(fit_max_svl_2$aic, fit_max_svl_3$aic, fit_max_svl_4$aic, rep(fit_max_svl_5$aic, 3))
phylolm_tab$sigma2 <- c(fit_max_svl_2$sigma2, fit_max_svl_3$sigma2, fit_max_svl_4$sigma2, rep(fit_max_svl_5$sigma2, 3))
phylolm_tab$adjusted_rsq <- c(fit_max_svl_2$adj.r.squared, fit_max_svl_3$adj.r.squared, fit_max_svl_4$adj.r.squared, rep(fit_max_svl_5$adj.r.squared, 3))
phylolm_tab$formula <- c(fit_max_svl_2$formula, fit_max_svl_3$formula, fit_max_svl_4$formula, fit_max_svl_5$formula, fit_max_svl_5$formula, fit_max_svl_5$formula)

phylolm_tab <- phylolm_tab %>% dplyr::select(formula, coefficients.Estimate, coefficients.StdErr, -coefficients.lowerbootCI, -coefficients.upperbootCI, coefficients.t.value,
                                             coefficients.p.value, lambda, sigma2, adjusted_rsq, aic)
phylolm_tab$formula <- as.character(phylolm_tab$formula)

# Body shape model fit
mvmorph_coef_df <- as.data.frame(rbind(fit_soil$coefficients[-1,], fit_temp$coefficients[-1,], fit_arid$coefficients[-1,], fit_all$coefficients[-1,]))
mvmorph_coef_df$terms <- c(aov_soil_bd$terms, aov_temp_bd$terms, aov_arid_bd$terms, aov_mult_bd$terms)
mvmorph_coef_df$model <- c(fit_soil$formula, fit_temp$formula, fit_arid$formula, fit_all$formula, fit_all$formula, fit_all$formula)
mvmorph_bd_manova_df <- data.frame(
  model_name = c(aov_soil_bd$terms, aov_temp_bd$terms, aov_arid_bd$terms, aov_mult_bd$terms),
  test_stat = c(aov_soil_bd$stat, aov_temp_bd$stat, aov_arid_bd$stat, aov_mult_bd$stat), 
  p_value = c(aov_soil_bd$pvalue, aov_temp_bd$pvalue, aov_arid_bd$pvalue, aov_mult_bd$pvalue)
)

mvmorph_body_table <- cbind(mvmorph_coef_df, mvmorph_bd_manova_df)
mvmorph_body_table$lambda <- c(fit_soil$param, fit_temp$param, fit_arid$param, fit_all$param, fit_all$param, fit_all$param)
mvmorph_body_table <- mvmorph_body_table %>% dplyr::select(model, terms, test_stat, p_value, lambda, everything(), -model_name)
mvmorph_body_table$model <- as.character(mvmorph_body_table$model)

# Head shape model fit
head_coef_df <- as.data.frame(rbind(fit_head_soil$coefficients[-1,], fit_head_temp$coefficients[-1,], fit_head_arid$coefficients[-1,], fit_head_all3$coefficients[-1,]))
head_coef_df$terms <- c(aov_soil_hd$terms, aov_temp_hd$terms, aov_arid_hd$terms, aov_mult_hd3$terms)
head_coef_df$model <- c(fit_head_soil$formula, fit_head_temp$formula, fit_head_arid$formula, 
                        fit_head_all$formula, fit_head_all$formula, fit_head_all$formula, 
                        fit_head_all$formula, fit_head_all$formula, fit_head_all$formula,
                        fit_head_all$formula, fit_head_all$formula, fit_head_all$formula,
                        fit_head_all$formula, fit_head_all$formula, fit_head_all$formula,
                        fit_head_all$formula, fit_head_all$formula)
# Manova results
hd_manova_df <- data.frame(
  model_name = c(aov_soil_hd$terms, aov_temp_hd$terms, aov_arid_hd$terms, aov_mult_hd3$terms),
  test_stat = c(aov_soil_hd$stat, aov_temp_hd$stat, aov_arid_hd$stat, aov_mult_hd3$stat), 
  p_value = c(aov_soil_hd$pvalue, aov_temp_hd$pvalue, aov_arid_hd$pvalue, aov_mult_hd3$pvalue)
)
mvmorph_head_table <- cbind(head_coef_df, hd_manova_df)

# Lambda parameter estimate
mvmorph_head_table$lambda <- c(fit_head_soil$param, fit_head_temp$param, fit_head_arid$param, rep(fit_head_all3$param, 14))

mvmorph_head_table <- mvmorph_head_table %>% dplyr::select(model, model_name, test_stat, p_value, lambda, everything(), -terms)
mvmorph_head_table$model <- as.character(mvmorph_head_table$model)

# Write out as csv
write.csv(phylolm_tab, file = "manuscript_word/supplements/phylolm_svl_table.csv", row.names = FALSE)
write.csv(mvmorph_body_table, file = "manuscript_word/supplements/mvmorph_body_table.csv", row.names = FALSE)
write.csv(mvmorph_head_table, file = "manuscript_word/supplements/mvmorph_head_table.csv", row.names = FALSE)

## Combine plots together

pdf(file = "output/PC1_pred.pdf", width = 11, height = 8.5)
par(mfrow=c(2,2))
plot(body_pca_results$scores[,1] ~ anilios_data$mean_bulk, bty ="n", pch=20, cex = 1.4,
     xlab = "Soil density", ylab = "Body PC1")
text(body_pca_results$scores[,1] ~ anilios_data$mean_bulk, labels = names(body_pca_results$scores[,1]))

plot(body_pca_results$scores[,1] ~ anilios_data$temp_mean, bty ="n", pch=20, cex = 1.4,
     xlab = "Temperature", ylab = "Body PC1")
text(body_pca_results$scores[,1] ~ anilios_data$temp_mean, labels = names(body_pca_results$scores[,1]))
plot(-head_pca_results$scores[,1] ~ anilios_data$mean_bulk, bty ="n", pch=20, cex = 1.4,
     xlab = "Soil density", ylab = "Head PC1")
text(-head_pca_results$scores[,1] ~ anilios_data$mean_bulk, labels = names(head_pca_results$scores[,1]))

plot(-head_pca_results$scores[,1] ~ anilios_data$temp_mean, bty ="n", pch=20, cex = 1.4,
     xlab = "Temperature", ylab = "Head PC1")
text(-head_pca_results$scores[,1] ~ anilios_data$temp_mean, labels = names(head_pca_results$scores[,1]))
dev.off()

plot(body_pca_results$scores[,1], head_pca_results$scores[,1],
     ylab = "Body PC1", xlab = "Head PC1")






-----
  
  #  GEOMORPH style on head shape
  load('data/script_generated_data/dorsal_head_shape.rda')
# Array of mean dorsal landmarks from gpa and symmetric analyses

rownames(sp_means_dorsal) <- gsub(x = rownames(sp_means_dorsal), pattern = "^", replacement = "Anilios_")
means_2d_dorsal <- sp_means_dorsal[rownames(sp_means_dorsal) %in% anilios_tree$tip.label,]
means_2d_dorsal <- means_2d_dorsal[match(anilios_tree$tip.label, rownames(means_2d_dorsal)), ]

# Turn head landmark traits 
Y_head <- as.matrix(means_2d_dorsal[-1])
head_traits <- list(Y_head=Y_head)

fit_head_1 <- mvgls(Y_head~1, data=head_traits, tree=anilios_tree, model="BM", penalty="RidgeArch")
fit_head_2 <- mvgls(Y_head~1, data=head_traits, tree=anilios_tree, model="OU", penalty="RidgeArch")
fit_head_3 <- mvgls(Y_head~1, data=head_traits, tree=anilios_tree, model="EB", penalty="RidgeArch")

GIC(fit_head_1); GIC(fit_head_2); GIC(fit_head_3) # BM has the lowest GIC score for head traits

phylo_signal_head <- mvgls(Y_head~1, data=head_traits, tree=anilios_tree, model="lambda", penalty="RidgeArch")
summary(phylo_signal_head)

### Using geomorph and 3D array
# Subset only species in tree from 3D array
dimnames(means_3d_dorsal)[[3]] <- paste("Anilios_", dimnames(means_3d_dorsal)[[3]], sep = "")

# Subet only species in tree
HeadLMs <- means_3d_dorsal[,, dimnames(means_3d_dorsal)[[3]] %in% anilios_tree$tip.label]
HeadLMs <- HeadLMs[,,match(anilios_tree$tip.label, dimnames(HeadLMs)[[3]])]

# Geomorph dataframe
GDF.Head <- geomorph::geomorph.data.frame(shape = HeadLMs, phy = anilios_tree)

## Phylogenetic signal Kmulti of head shape
PS.head <- geomorph::physignal(A = GDF.Head$shape, phy = anilios_tree, iter = 999)
summary(PS.head) # K multi = 1.3434, P-Value = 0.001







# Fit BM ------------------------------------------------------------------



hist(body.pc); hist(head.pc)


fitBM_func <- function(.tree, .dat, .model){
  fit_obj <- geiger::fitContinuous(phy = .tree, dat = .dat, model = paste(.model))
  return(fit_obj)
}

# Make a list of all variables to fit
anilios_vars <- list(body = body.pc, head = head.pc, soil = soil.bulk, 
                     sand = sand.pc, temp = temp_m, prec = prec_m)

BM_fit <- lapply(X = anilios_vars, FUN = fitBM_func, .tree = anilios_tree, .model = "BM")
lapply(X = anilios_vars, FUN = fitBM_func, .tree = anilios_tree, .model = "EB")
lapply(X = anilios_vars, FUN = fitBM_func, .tree = anilios_tree, .model = "OU")


fitBM_bpc <- geiger::fitContinuous(phy = anilios_tree, dat = body.pc, model = "BM")
fitBM_bpc
# sigma^2 = 0.14 indicate that under Brownian process, we expect a large number of 
# independently evolving lineages to accumulate the variance among each other of 
# 0.14 after one unit of time.
fitBM_bpc$opt$z0
# chi-0 -0.24 is the most likely state at the root of the tree, under this model



# Phylogenetic signal -----------------------------------------------------

# Blomberg's K is normalised ratio comparing variance among clades on the tree to the
# variance within clades. 
# If variance among clade is high (compared to within clades) then phylosig is high
# If variance within clade is high (compared to among clade), then phylosig is low

phytools::phylosig(tree = anilios_tree, x = body.pc)
## Test for significant phylosig using Blomberg's K
K_bodypc <- phytools::phylosig(tree = anilios_tree, x = body.pc, 
                               test = TRUE, nsim = 10000)
K_bodypc
par(cex = 0.8, mar=c(5.1,4.1,2.1,2.1))
plot(K_bodypc,las=1,cex.axis=0.9)

K_headpc <- phytools::phylosig(tree = anilios_tree, x = head.pc, 
                               test = TRUE, nsim = 10000)
plot(K_headpc,las=1,cex.axis=0.9)

K_soil <- phytools::phylosig(tree = anilios_tree, x = soil.bulk, 
                             test = TRUE, nsim = 10000)
plot(K_soil,las=1,cex.axis=0.9)

# All three traits show that we can reject null hypothesis that the traits are randomly arrayed on the phylogeny by chance.

## Is the phylogenetic signal *less* than expected under BM? 
## Simulate 10000 datasets
nullX <- fastBM(anilios_tree, nsim = 10000)
## For each carry out a test for phyl sig and accumulate these into vector
nullK <- apply(nullX, 2, FUN = phylosig, tree=anilios_tree)
# Calculate p-values
# Because our K value is greater than 1, we count the times when simulated data have greater values than observed values. 
Pval_bodypc <- mean(nullK>=K_bodypc$K)
Pval_headpc <- mean(nullK>=K_headpc$K)
Pval_soil <- mean(nullK>=K_soil$K)
Pval_bodypc; Pval_headpc; Pval_soil


phytools::phylosig(tree = anilios_tree, x = temp_m, method = "K", test = T)
phytools::phylosig(tree = anilios_tree, x = prec_m, method = "K", test = T)
phytools::phylosig(tree = anilios_tree, x = sand.pc, method = "K", test = T)


dev.off()
hist(c(nullK,K_bodypc$K),breaks=30,col="lightgray",
     border="lightgray",main="",xlab="K",las=1,
     cex.axis=0.7,cex.lab=0.9,ylim=c(0,4000))
## actual value as an arrow
arrows(x0=K_bodypc$K,y0=par()$usr[4],y1=0,length=0.12,
       col=make.transparent("blue",0.5),lwd=2)
text(K_bodypc$K,0.96*par()$usr[4],
     paste("Body PC (P = ",
           round(Pval_bodypc,4),")",sep=""),
     pos=4,cex=0.8)
## plot for head PC
arrows(x0=K_headpc$K,y0=par()$usr[4],y1=0,length=0.12,
       col=make.transparent("red",0.5),lwd=2)
text(K_headpc$K,0.96*par()$usr[4],
     paste("Head PC K (P = ",
           round(Pval_headpc,4),")",sep=""),
     pos=4,cex=0.8)
## Plot for soil
arrows(x0=K_soil$K,y0=par()$usr[4],y1=0,length=0.12,
       col=make.transparent("salmon",0.5),lwd=2)
text(K_soil$K,0.96*par()$usr[4],
     paste("soil K (P = ",
           round(Pval_soil,4),")",sep=""),
     pos=4,cex=0.8)

## INTERPRETATION :: 
# Phylo signal for body and head pc scores are significantly lower than what we'd expect under Brownian evolution. 
# For soil bulk density, we obtain phylogenetic signal values that are as small as the observed data, even when data are simulated under BM.


## Pagel's lambda
# Lambda is scaling coefficient for the off-diagonal elements in the expected correlations among species
# Lambda < 1 correspond to *less* phylogenetic signal than expected under BM. Unlike K, lambda is not generally well defined outside the range of (0,1).
# Lambda is more appropriate for detecting phylosignal that is lower than expected under BM rather than higher. 
# Null is lambda = 0, so when lambda > 1 phylogenetic signal as expected under BM

lambda_bodypc <- phytools::phylosig(tree = anilios_tree, x = body.pc, method = "lambda", test = TRUE)
lambda_headpc <- phytools::phylosig(tree = anilios_tree, x = head.pc, method = "lambda", test = TRUE)
lambda_soil <- phytools::phylosig(tree = anilios_tree, x = soil.bulk, method = "lambda", test = TRUE)
lambda_bodypc; lambda_headpc; lambda_soil
# Our lambda values are > 1, and the P values from above code confirms that we can reject the null that lambda = 0 (no phylogenetic signal)

# Test null hypothesis that lambda = 1, where there *is* phylogenetic signal
LR_body <- -2*(lambda_bodypc$lik(1) - lambda_bodypc$logL)
Pval_lambda_body <- pchisq(LR_body, df=1, lower.tail = F)

LR_head <- -2*(lambda_headpc$lik(1) - lambda_headpc$logL)
Pval_lambda_head <- pchisq(LR_head, df=1, lower.tail = F)

LR_soil <- -2*(lambda_soil$lik(1) - lambda_soil$logL)
Pval_lambda_soil <- pchisq(LR_soil, df=1, lower.tail = F)


Pval_lambda_body; Pval_lambda_head; Pval_lambda_soil

# We can reject the null hypothesis that lambda is = 1 (there IS phylogenetic signal) for head and body but not for soil. 
# This is because the value is significantly different from 1. So we rejected null for both lambda = 1 and lambda = 0 for head and body!? 

## Fit alternative fit continuous models

# Early Burst model
fitEB_body <- geiger::fitContinuous(anilios_tree, body.pc, model = "EB")
fitEB_head <- geiger::fitContinuous(anilios_tree, head.pc, model = "EB")
fitEB_soil <- geiger::fitContinuous(anilios_tree, soil.bulk, model = "EB")

# OU model
fitOU_body <- geiger::fitContinuous(anilios_tree, body.pc, model = "OU")
fitOU_head <- geiger::fitContinuous(anilios_tree, head.pc, model = "OU")
fitOU_soil <- geiger::fitContinuous(anilios_tree, soil.bulk, model="OU")

## Model comparisons
aic_body <- setNames(c(AIC(fitBM_bpc), AIC(fitEB_body), AIC(fitOU_body)), c("BM","EB","OU"))
aic.w(aic_body)
aic_head <- setNames(c(AIC(fitBM_hpc), AIC(fitEB_head), AIC(fitOU_head)), c("BM","EB","OU"))
aic.w(aic_head)
aic_soil <- setNames(c(AIC(fitBM_soil), AIC(fitEB_soil), AIC(fitOU_soil)), c("BM","EB","OU"))
aic.w(aic_soil)






# Function to test phylogenetic signal (both K and lambda) and plot continuous variable
sigFunc <- function(.tree, .data) {
  traitlabel <- stringr::str_extract(deparse(substitute(.data)), pattern = '\\$(.*)')
  dat <- setNames(.data, .tree$tip.label)
  sig <- list(phytools::phylosig(tree = .tree, x = dat, method = "K", test = T),
              phytools::phylosig(tree = .tree, x = dat, method = "lambda", test = T))
  contmap <- phytools::contMap(tree = .tree, x = dat, plot = F)
  
  ## what is the length of the current color ramp?
  n <- length(contmap$cols)
  ## change to blue -> red
  # contmap$cols[1:n] <- colorRampPalette(c("blue", "yellow", "red"), space = "Lab")(n)
  contmap$cols[1:n] <- colorRampPalette(brewer.pal(6, "RdYlBu"), space = "Lab")(n)
  .p <- plot(contmap, lwd = 3, outline = T, direction = "rightwards")
  
  # *** FIGURE WAY TO PUT DOT OF ENVIRONMENT ***
  
  # plot(phytools::setMap(contmap, invert=T), lwd = 3.5, outline = T, direction = "upwards")
  # print stats as annotation
  .p
  sig_stat <- lapply(list(sig[[1]]$K, sig[[1]]$P, sig[[2]]$lambda, sig[[2]]$P), round, 2)
  K <- paste('Phylosig K =', sig_stat[[1]], 'p=', sig_stat[[2]])
  Lambda <- paste('Lambda =', sig_stat[[3]], 'p=', sig_stat[[4]])
  text(x = 8, y = 1, labels = K)
  text(x = 8, y = 0, labels = Lambda)
  # label what trait
  text(x = 8, y = 2, labels = traitlabel)
  return(list(sig, .p))
}


# Test the function
sigFunc(.tree = anilios_tree, .data = anilios_data$body_PC1)

# List of continuous traits to fit to custom function
cont_traits <- list(mean_PC1 = anilios_data$mean_PC1, mean_PC2 = anilios_data$mean_PC2, 
                    body_PC1 = anilios_data$body_PC1, body_PC2 = anilios_data$body_PC2, 
                    soil = anilios_data$mean_bulk, sand = anilios_data$mean_sand_per,
                    temp = anilios_data$temp_mean, prec = anilios_data$prec_mean)

# apply to all traits in the cont_traits list
physig_list <- lapply(cont_traits, sigFunc, .tree = anilios_tree) 

# Name the elements
names(physig_list) <- names(cont_traits)

# Print
par(mfrow=c(1,2))
print_physig <- cont_traits <- list(mean_PC1 = anilios_data$mean_PC1, 
                                    body_PC1 = anilios_data$body_PC1, 
                                    soil = anilios_data$mean_bulk)

dev.off()
contmap_b <- phytools::contMap(tree = anilios_tree, x = body.pc, plot = F)
contmap_h <- phytools::contMap(tree = anilios_tree, x = head.pc, plot = F)
contmap_s <- phytools::contMap(tree = anilios_tree, x = soil.bulk, plot = F)
contmap_temp <- phytools::contMap(tree = anilios_tree, x = temp_m, plot = F)
contmap_p <- phytools::contMap(tree = anilios_tree, x = prec_m, plot = F)

contmap_sand <- phytools::contMap(tree = anilios_tree, x = sand.pc, plot = F)

## what is the length of the current color ramp?
n <- length(contmap_b$cols)
contmap_b$cols[1:n] <- colorRampPalette(brewer.pal(6, "RdYlBu"), space = "Lab")(n)
contmap_h$cols[1:n] <- colorRampPalette(brewer.pal(6, "RdYlBu"), space = "Lab")(n)
contmap_s$cols[1:n] <- colorRampPalette(brewer.pal(6, "RdYlBu"), space = "Lab")(n)
contmap_sand$cols[1:n] <- colorRampPalette(brewer.pal(6, "RdYlBu"), space = "Lab")(n)
contmap_temp$cols[1:n] <- rev(colorRampPalette(brewer.pal(6, "RdYlBu"), space = "Lab")(n))
contmap_p$cols[1:n] <- colorRampPalette(brewer.pal(6, "RdYlBu"), space = "Lab")(n)

dev.off()
pdf(file = "output/contmap_phylosig.pdf", width = 11, height = 8.5)
par(mfrow=c(3,2))
plot(contmap_b, lwd = 3, outline = T, direction = "rightwards", ftype = "off", leg.txt="Body PC K=1.3 P=0.001")
plot(contmap_h, lwd = 3, outline = T, direction = "leftwards", ftype = "off", leg.txt="Head PC K=1.48 P=0")
plot(contmap_sand, lwd = 3, outline = T, direction = "rightwards", ftype = "off", leg.txt="Sand% K=1.17 P=0.002")
plot(contmap_s, lwd = 3, outline = T, direction = "leftwards", ftype = "off", leg.txt="Bulk density K=1.13, P=0.002")
plot(contmap_temp, lwd = 3, outline = T, direction = "rightwards", ftype = "off", leg.txt="Mean Temp K=0.91, P=0.027")
plot(contmap_p, lwd = 3, outline = T, direction = "leftwards", ftype = "off", leg.txt="Mean Prec K=1.3, P=0.001")
dev.off()
