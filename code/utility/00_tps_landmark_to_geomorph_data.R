# Filename: 00_tps_landmark_to_geomorph_data.R
# Sarin Tiatragul 
# December 2020 - modified 2021-09-03 | 2023-01-31
# No. 

# Must be preceded by
# source('code/utility/00_linear_measurements_shaperatio.R')

# Based on "Addition by Subtraction: Improving geomorph with fewer functions" - Michael Collyer 2020
# this script prepares .TPS and .NTS landmark data for model fitting

# INSTALL REQUIRED LIBRARIES-----------------------------------------------

# install.packages("geomorph", dependencies = TRUE)
# install.packages("devtools", dependencies = TRUE)
# devtools::install_github("geomorphR/geomorph",ref = "Stable")

# LIBRARIES ---------------------------------------------------------------

library(dplyr); library(tidyr); library(readr);
library(janitor); library(stringr); library(geomorph)
'%notin%' <- Negate('%in%')

# INPUTS ------------------------------------------------------------------
# Blindsnake forehead landmark data digitised and prepare using TPS series software
# by F. James Rolfe

# Landmark TPS input
tps_input_d <- "data/dorsal/20231107_lm_dorsal_aligned.tps"

# Centroid size NTS input
centroid_input_NTS <- "data/dorsal/20231107_lm_dorsal_centroid_size.NTS"

# CSV file containing file names of photos used in landmarking 
landmark_list_csv <- "data/dorsal/dorsal_landmark_list.csv"

# CSV file containing body shape proportions/ratios
shape_ratios_df <- read.csv("data/script_generated_data/shape_ratios/blindsnake_logbodyshape_ratio.csv")

# READ .TPS DATA ----------------------------------------------------------

#  Read in TPS data using geomorph readlandmark

tps_input <- geomorph::readland.tps(paste(tps_input_d), specID = "ID")

# Redefine four curves and anchor points
curve1 <- c(9, 24:15, 3)
curve2 <- c(3, 14:11, 1)
curve3 <- c(1, 25:28, 2)
curve4 <- c(2, 29:38, 8)

# New landmark coordinates with 26 instead of 38

p_landmark = 26
k_landmark = 2

new.coords <- array(NA, dim = c(p_landmark, k_landmark, dim(tps_input)[3]))
dimnames(new.coords)[[3]] <- dimnames(tps_input)[[3]]

## Reduce landmarks
for(i in 1:dim(tps_input)[3]){
  tmp <- tps_input[,,i] 

  new_curve1 <- digit.curves(start = tmp[9,], curve = tmp[curve1,], nPoints = 5, closed = FALSE)
  new_curve2 <- digit.curves(start = tmp[3,], curve = tmp[curve2,], nPoints = 3, closed = FALSE)
  new_curve3 <- digit.curves(start = tmp[1,], curve = tmp[curve3,], nPoints = 3, closed = FALSE)
  new_curve4 <- digit.curves(start = tmp[2,], curve = tmp[curve4,], nPoints = 5, closed = FALSE)
  
  # Assign these ones in first
  new.coords[1:5,,i] <- tmp[c(4,6,5,7,10),]
  new.coords[6:12,,i] <- new_curve1
  new.coords[13:16,,i] <- new_curve2[-1,]
  new.coords[17:20,,i] <- new_curve3[-1,]
  new.coords[21:26,,i] <- new_curve4[-1,]
  
}

# Define sliders again
sliding_sm <- rbind(define.sliders(landmarks = 6:12, nsliders = 5, write.file = FALSE),
                    define.sliders(landmarks = 12:16, nsliders = 5, write.file = FALSE),
                    define.sliders(landmarks = 16:20, nsliders = 5, write.file = FALSE), 
                    define.sliders(landmarks = 20:26, nsliders = 5, write.file = FALSE))


# Test to make sure this is what we want
plot(new.coords[,,4], cex = 0.1)
text(new.coords[,,4], labels = c(1:p_landmark))

# PROCRUSTES SUPERIMPOSITION ----------------------------------------------
# this is the essential step for shape analysis because we need to remove effects of 
# scale (make them roughly the same size), location, and orientation
# Our semilandmarks have already been slid using minimum bending energy in tpsRel

#  perform generalised procrustes analysis of points, curves, and surface
y.gpa <- geomorph::gpagen(A = new.coords, curves = sliding_sm, ProcD = FALSE)

# SYMMETRIC COMPONENTS -- LANDMARK PAIRS ----------------------------------
# designate landmark pairs for symmetric component for DORSAL VIEW
# need this matrix to tell bilat.symmetry() which landmarks mirror each other

part1 <- c(6:15, 4, 3) # these are landmarks and semilandmarks that are on one side
part2 <- c(26:17, 2, 1) # landmarks and semilandmarks on the other side that is mirroring 

lm_pairs <- as.matrix(cbind(part1, part2))

#  Analyse directional and fluctuating asymmetry for bilat symm objects
sym_component <- geomorph::bilat.symmetry(A = y.gpa$coords, object.sym = TRUE, 
                                          land.pairs = lm_pairs, 
                                          ind = dimnames(y.gpa$coords)[[3]])

summary(sym_component) # results from analysis of bilateral symmetry

#  Create an object for Symmetric components of shape (Y the best object to analyse)
y_sym_comp <- sym_component$symm.shape

# Can check by printing
# plotRefToTarget(mshape(y_sym_comp), mshape(y_sym_comp[,,1]))

#  Turn into 2d array --- don't need to turn this into two.d.array for fitting purposes
y_sym_comp_2d <- geomorph::two.d.array(y_sym_comp) # turn into 2d array

#  Read in centroid size but ignore first three rows of text file
c_size_nts <- read.delim(file = paste(centroid_input_NTS), header = FALSE, skip = 3) 

#  Read list of image file name
classifier <- readr::read_csv(paste(landmark_list_csv), col_names = c("no", "image_name")) # give name to columns

# COMBINE CENTROID SIZE WITH EACH REGO NUMBER -----------------------------

#  Append ID, start at 0 and make ID as character
row_length <- length(classifier$no)-1
classifier$id <- as.character(0:row_length) # make id character instead of double

#  Append centroid size data from gpagen (this Csize is already centred)
classifier$c_size <- y.gpa$Csize
classifier$c_size_old <- c_size_nts[, 1]  # add centroid size to data

#  Append image name (file name of image used in tpsDig)
classifier$species <- stringr::str_extract(classifier$image_name, 
                                           pattern = regex("[A-z]+"))

#  Get rego number from image file
## regex to extract museum registration number from image file name
classifier$source_number <- str_extract(string = classifier$image_name,
                                        pattern = "(?<=[/-])[A-z](.*?){2,5}(?=[/-]|[/.])") 

#  Capitalise rego number
## match first character and replace with uppercase
classifier$source_number <- gsub("(^|[[:space:]])([[:alpha:]])", "\\1\\U\\2", 
                                 as.character(classifier$source_number), perl = TRUE) 

#  Get rego number without leading zeros for registration that do not use leading zeros
classifier$reg_number <- gsub("(?<![0-9])0+", "", classifier$source_number, perl = TRUE) 

#  Get plain number from registration
classifier$plain_number <- gsub("[^0-9]", "", as.character(classifier$reg_number), perl = TRUE) # remove first letter [A-]
classifier$plain_number <- gsub("^0+(?!$)", "", as.character(classifier$plain_number), perl = TRUE) # remove leading zeros in the string if have

classifier$source_number[classifier$source_number == "S0364"] <- "SMZ0364"
shape_ratios_df$plain_number[shape_ratios_df$plain_number == "0364"] <- "364"
#  only get information for specimens that are in the .TPS file
id_number_df <- dimnames(tps_input)[[3]] %>% as_tibble() # ID of specimens in the .TPS file

#  make centroid size dataframe
centroid_size_df <- classifier %>% 
  dplyr::left_join(id_number_df, by = c('id' = 'value')) %>% 
  dplyr::select(-image_name, species) %>% 
  dplyr::left_join(shape_ratios_df[,c("plain_number", "sex")], by = 'plain_number')

#  corrections for some numbers that were mistaken in photos
centroid_size_df$plain_number[which(centroid_size_df$plain_number == '4830')] <- NA # from ANWC but no X-ray data, will omit this specimen
centroid_size_df$plain_number[which(centroid_size_df$plain_number == '74795')] <- NA # did not take any measurements from WAM
centroid_size_df$plain_number[which(centroid_size_df$plain_number == '78320')] <- '23870' # correction
centroid_size_df$plain_number[which(centroid_size_df$plain_number == '21047')] <- '16047' # wrong rego in photo file
centroid_size_df$plain_number[which(centroid_size_df$plain_number == '412')] <- '4012' # wrong rego in photo file

# Fit allometry to get size free shape

fit.allometry0 <- procD.lm(y_sym_comp_2d ~ centroid_size_df$c_size, iter = 999, type = "III")
shape_resids <- fit.allometry0$residuals
# There is some allometry effect

# Check for sexual dimorphism ---------------------------------------------

y_sym_comp_2d_sex_di_check <- tibble(reg = centroid_size_df$source_number, 
                                     spe = centroid_size_df$species,
                                     c_size = centroid_size_df$c_size,
                                     reg_number = centroid_size_df$reg_number,
                                     plain_number = centroid_size_df$plain_number,
                                     sex = centroid_size_df$sex,
                                     y_sym_comp_2d) %>% 
  dplyr::full_join(shape_ratios_df[,c("species", "plain_number")], by = 'plain_number') %>% 
  dplyr::filter(sex %in% c('f', 'm'))
  
exclude_species_head <- y_sym_comp_2d_sex_di_check %>% dplyr::group_by(species, sex) %>% dplyr::summarise(n = n()) %>% dplyr::filter(n < 3) %>% dplyr::distinct(species) %>% c()

subset_dimorph_head <- y_sym_comp_2d_sex_di_check[y_sym_comp_2d_sex_di_check$species %notin% exclude_species_head$species,]
subset_dimorph_head %>% distinct(species)

# Fit model with species and sex to see what is the bigger factor of variation
gdf <- geomorph.data.frame(coords = subset_dimorph_head$y_sym_comp_2d,
                           species = subset_dimorph_head$species,
                           c_size = subset_dimorph_head$c_size,
                           sex = subset_dimorph_head$sex)

y_sym_comp_3d <- y_sym_comp

# Type III Procrustes ANOVA to determine the effect of the species, sex
# Take into account allometry
fit.sp.sex <- geomorph::procD.lm(coords ~ c_size + species * sex, data = gdf, iter = 999, SS.type = "I")
summary(fit.sp.sex)
## Variation of head shape is driven by centroid size and species but not sex nor the interaction between species:sex

# Calculate the pairwise comparisons
head.group <- interaction(gdf$species, gdf$sex)
PW1 <- pairwise(fit.sp.sex, groups = head.group)

head.pairwise <- summary(PW1, confidence = 0.95, test.type = "dist")
head.pair.df <- head.pairwise$summary.table

# Create a vector of formatted strings
head.pair.sp <- unlist(lapply(unique(gdf$species), function(x) paste0(x, ".f:", x, ".m")))

head.pair.stat <- head.pair.df[rownames(head.pair.df) %in% head.pair.sp, ]
head.pair.stat$species <- paste('Anilios', gsub("^(.*?)\\..*", "\\1", rownames(head.pair.stat)))

sex_dimorph_result_table <- head.pair.stat
colnames(sex_dimorph_result_table) <- c("distance", "UCL (95%)", "Z", "P", "Species")

sex_dimorph_result_table <- sex_dimorph_result_table[,c(5, 1:4)]
sex_dimorph_result_table[,c(2:5)] <- round(sex_dimorph_result_table[,c(2:5)], 3)
# How many species did we consider? 
unique(sex_dimorph_result_table$Species)
write.csv(sex_dimorph_result_table, file = 'manuscript_word/supplements/supp_contrast_headshape.csv', row.names = FALSE)

# get names of species that are sexually dirmophic in head shape 
head.dimorphic.sp <- gsub("^(.*?)\\..*", "\\1", rownames(head.pair.stat[head.pair.stat$`Pr > d` < 0.05, ]))

# Repeat superimposition  -------------------------------------------------
# Repeat Procrustres superimposition after removing unguirostris
exclude_id <- centroid_size_df %>% filter((species %in% substr(head.dimorphic.sp, 1,3) & sex == "m"))

# # Create subset without sexually dimorphic species
filter.new.coords <- new.coords[,,-as.numeric(exclude_id$id)]

# Do GPA again 
y.gpa <- geomorph::gpagen(A = filter.new.coords, curves = sliding_sm, ProcD = FALSE)

# Bilat 
sym_component <- geomorph::bilat.symmetry(A = y.gpa$coords, object.sym = TRUE, 
                                          land.pairs = lm_pairs, 
                                          ind = dimnames(y.gpa$coords)[[3]])

#  Create an object for Symmetric components of shape (Y the best object to analyse)
y_sym_comp <- sym_component$symm.shape

# Turn into 2d array
y_sym_comp_2d <- geomorph::two.d.array(y_sym_comp) # turn into 2d array

## Filter centroid size data as well to match
centroid_size_df <- centroid_size_df[which(centroid_size_df$id %notin% as.numeric(exclude_id$id)),] 

y_sym_comp_2d_df <- tibble(reg = centroid_size_df$source_number, 
                           species = centroid_size_df$species,
                           c_size = centroid_size_df$c_size,
                           reg_number = centroid_size_df$reg_number,
                           plain_number = centroid_size_df$plain_number,
                           y_sym_comp_2d) %>%
  dplyr::full_join(shape_ratios_df[,c("genus", "species", "plain_number")], by = 'plain_number') %>% 
  dplyr::select(species.y, everything())

# Corrections 
y_sym_comp_2d_df$species.y[which(y_sym_comp_2d_df$species.x == "ery")] <- "erycinus"
y_sym_comp_2d_df$species.y[which(y_sym_comp_2d_df$species.x == "sub")] <- "subocularis"
y_sym_comp_2d_df$reg_number[which(y_sym_comp_2d_df$reg_number == "F113561_2023")] <- "UF113561"
y_sym_comp_2d_df$reg_number[which(y_sym_comp_2d_df$reg_number == "Z18963")] <- "ZMB18963"
# Still some missing but will ignore because maybe doesn't have body measurements

# Filter specimen we did not have head photographs for 
# This may be because the head was severed or not in good condition

y_sym_comp_2d_df_filtered <- y_sym_comp_2d_df %>% 
  dplyr::filter(!is.na(y_sym_comp_2d[,1]))
  # dplyr::filter(!is.na(shape_resids[,1]))

#  Turn object into into a 3d array
y_sym_comp_3d_array <- geomorph::arrayspecs(A = y_sym_comp_2d_df_filtered[,7], 
                                            p = p_landmark, k = k_landmark)

# Can manually check if the shapes are weird
# plot(y_sym_comp_3d_array[, , 37])

##  Subset for columns only for symmetric components for PCA and force into dataframe
y_sym_head <- as.data.frame(y_sym_comp_2d_df_filtered[ , grepl('y_sym_comp_2d', names(y_sym_comp_2d_df_filtered)) ] )
# y_sym_head <- y_sym_comp_2d_df_filtered[ , grepl('shape_resid', names(y_sym_comp_2d_df_filtered)) ] %>% as.data.frame()

# PCA HEAD SHAPE ----------------------------------------------------------

#  Take PC1 and PC2 for head already using residuals so no need to scale or center
# y_sym_head_pc <- prcomp(y_sym_head, scale = F, center = F)

#  Take PC1 and PC2 for head already using symmetric components
y_sym_head_pc <- prcomp(y_sym_head, scale = T, center = T)

#  Visualise where individuals land on PC axis to check if there's any outlier
factoextra::fviz_pca_ind(y_sym_head_pc)

#  Calculate variance explained by each PC component
factoextra::fviz_eig(y_sym_head_pc)

#  Make dataframe of PC scores from PCA on head shape
y_sym_head_pc_score <- data.frame(head_PC1 = y_sym_head_pc$x[, 1],
                                  head_PC2 = y_sym_head_pc$x[, 2])

ind_head_pc <- cbind(y_sym_comp_2d_df_filtered[, c("reg_number", "genus", "species.y", "c_size")], 
                     y_sym_head_pc_score) 
# %>% dplyr::filter(!is.na(species.y))
ind_head_pc$genus[is.na(ind_head_pc$genus)] <- "Anilios"

# Calculate mshape --------------------------------------------------------
# mean shape of aligned specimens

sp.group <- factor(y_sym_comp_2d_df_filtered$species.y)

sp.coords <- geomorph::coords.subset(A = y_sym_comp_3d_array, group = sp.group)
names(sp.coords)
three_d_mshape <- lapply(sp.coords, mshape)

save(three_d_mshape, file = 'data/script_generated_data/three_d_mshape.rda')

# CALCULATE MEANS ---------------------------------------------------------
# Calculate mean shape per species
# because analysis down the line is on means

sym_mat <- y_sym_comp_2d_df_filtered %>%  
  dplyr::select(c_size, starts_with("y_sym")) %>%
  # dplyr::select(c_size, starts_with("shape_resid")) %>% 
  as.matrix()

#  Species mean resids
sp_means_dorsal <- aggregate(sym_mat, list(y_sym_comp_2d_df_filtered$species.y), FUN = mean) 
rownames(sp_means_dorsal) <- sp_means_dorsal$Group.1


# Species mean raw (not residuals) for plotting purposes
sp_means_dorsal_ref <- aggregate(y_sym_comp_2d, list(y_sym_comp_2d_df_filtered$species.y), FUN = mean) 
rownames(sp_means_dorsal_ref) <- sp_means_dorsal$Group.1

#  Principal component analysis
Y_PC_dorsal <- prcomp(sp_means_dorsal[,-c(1,2)])

# Variance explained in this mean dataset
factoextra::fviz_eig(Y_PC_dorsal)

#  Plot mean PCA
dev.off()
pdf(file = 'output/00_dorsal_head_pca_raw.pdf', width = 11.33, height = 8.5)
factoextra::fviz_pca_ind(Y_PC_dorsal, 
                         geom = c("point", "text"), pointsize = 2,
                         title = "Blindsnake head shape PCA",
                         # col.ind = "coord",
                         # gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
                         repel = TRUE     # Avoid text overlapping
)
dev.off()

save(Y_PC_dorsal, file = "data/script_generated_data/head_pca.rda")

#  Convert to dataframe 
Y_PC_dorsal_df <- data.frame(species = sp_means_dorsal$Group.1,
                             mean_PC1 = Y_PC_dorsal$x[, 1],
                             mean_PC2 = Y_PC_dorsal$x[, 2],
                             c_size = sp_means_dorsal$c_size)

plot(x = Y_PC_dorsal_df$mean_PC1, y= Y_PC_dorsal_df$mean_PC2)
text(x = Y_PC_dorsal_df$mean_PC1, y= Y_PC_dorsal_df$mean_PC2, labels = Y_PC_dorsal_df$species)
# CONVERT TO 3D ARRAY AND MATRIX------------------------------------------------

#  turn species means in 3d array
means_3d_dorsal <- arrayspecs(sp_means_dorsal[,-c(1,2)],p_landmark, k_landmark) 
means_3d_dorsal_ref <- arrayspecs(sp_means_dorsal_ref[,-1],p_landmark, k_landmark) 
# means_3d_dorsal <- arrayspecs(sp_means_dorsal[,-c(1,2)],38,2) 
# # add species name
# dimnames(means_3d_dorsal)[[3]] <- sp_means_dorsal$Group.1

Ypcs_matrix <- Y_PC_dorsal_df

# SAVE OUTPUT AS RDA FOR VISUALISATION ------------------------------------

save(Ypcs_matrix, means_3d_dorsal, sp_means_dorsal, sp_means_dorsal_ref, means_3d_dorsal_ref, ind_head_pc, file = "data/script_generated_data/dorsal_head_shape.rda")

# When not using residuals
save(Ypcs_matrix, means_3d_dorsal, sp_means_dorsal, sp_means_dorsal_ref, means_3d_dorsal_ref, ind_head_pc, file = "data/script_generated_data/dorsal_head_shape_raw.rda")
