# Filename: phylogeny_sqcl_subset.R
# Putter Tiatragul 
# June 2022
# this script subsets the phylogeny to match data I have. 

# LIBRARIES ---------------------------------------------------------------

library(phytools); library(ape)
library(dplyr); library(stringr)
library(MCMCtreeR); 
library(phyloch)

# Time calibrated species tree --------------------------------------------

phy_full_mcmc <- MCMCtreeR::readMCMCtree(inputPhy = 'data/v3_01_FigTree.tre', from.file = TRUE)
phy_full <- phy_full_mcmc$apePhy 

# Rename tips (for grypus we have three potential species and ligatus)
# phy_full$tip.label[which(phy_full$tip.label %in% c('Anilios_grypus_R108596','Anilios_grypus_R157297','Anilios_grypus_R55272'))] <- c('Anilios_grypusW_R108596', 'Anilios_grypusNW_R157297', 'Anilios_grypusET_R55272')

# Change Anilios polygrammicus to Sundatyphlops
phy_full$tip.label[which(phy_full$tip.label %in% c('Anilios_polygrammicus_R98715', 'Ramphotyphlops_multillineatus_ABTC148379', 'Typhlopidae_Indiotyphlops_braminus_I1343'))] <- c('Indotyphlops_braminus_I1343', 'Sundatyphlops_polygrammicus_R98715', 'Ramphotyphlops_multilineatus_R148379')

dev.off()
plot(phy_full)

# MANIPULATE TREE ---------------------------------------------------------
# Keep only tips of species

tip_keep <- c(phy_full$tip.label[stringr::str_detect(phy_full$tip.label, pattern = c("Anilios"))], 
              'Acutotyphlops_subocularis_R64768', 'Ramphotyphlops_multilineatus_R148379')

# sub tree gets rid of number and SH
sub_phy <- phy_full %>%
  keep.tip(., tip_keep)

plot(sub_phy)
sub_phy$tip.label

shape_tree <- drop.tip(phy = sub_phy, tip = c('Anilios_grypus_R157297', 'Anilios_grypus_R55272', 'Anilios_unguirostris_R115861',
                                              'Anilios_unguirostris_R180002', 'Anilios_unguirostris_R21669', 'Anilios_ligatus_R31019',
                                              'Indotyphlops_braminus_I1343', 'Sundatyphlops_polygrammicus_R98715', 
                                              'Ramphotyphlops_multilineatus_R148379', 'Acutotyphlops_subocularis_R64768'))

# Force ultrametric 
sub_phy <- phytools::force.ultrametric(sub_phy,"extend")
shape_tree <- phytools::force.ultrametric(shape_tree,"extend")

# Clean up tip label
sub_phy$tip.label <- gsub("_[A-Z][0-9]+", '', sub_phy$tip.label) # rename so it so we have tips labels without numbers
shape_tree$tip.label <- gsub("_[A-Z][0-9]+", '', shape_tree$tip.label) # rename so it so we have tips labels without numbers

sub_phy$edge.length <- sub_phy$edge.length * 100


dev.off()
pdf(file = "output/mcmctree_axis_geo.pdf", width = 13.33, height = 7.5)
plot.phylo(sub_phy)
axisPhylo()
data(strat2012)
axisGeo(GTS = strat2012, unit = c("epoch", "period"), col = c('#767171', '#D46112'), 
        texcol = 'white', gridty = 0)
dev.off()

pdf(file = "output/ltt.pdf", width = 7.5, height = 7.5)
ltt(sub_phy, drop.extinct = TRUE, log.lineages = FALSE)
dev.off()


# Prepare painted tree
# http://blog.phytools.org/2020/04/can-you-show-clades-in-different-colors.html
plotTree(sub_phy, ftype = 'i')
nodelabels(frame = 'circ', bg = 'white', cex = 0.8)
edgelabels(bg = 'red', cex = 0.8)
cladelabels(sub_phy, 
            node = c(84, 82, 75, 71, 60, 55), 
            text = c("clade1", "clade2", "clade3", "clade4", "clade5", "clade6"))

sub_phy



# Paint subtree
blindsnake_tree <- phytools::paintSubTree(tree = sub_phy, node = 84, state = "#648FFF")
blindsnake_tree <- phytools::paintSubTree(tree = blindsnake_tree, node = 60, state ="#DC267F")
blindsnake_tree <- phytools::paintSubTree(tree = blindsnake_tree, node = 51,state = "#1FFA1A")
# blindsnake_tree <- phytools::paintSubTree(tree = blindsnake_tree, node = 68, state ="#DC267F")
blindsnake_tree <- phytools::paintSubTree(tree = blindsnake_tree, node = 75, state = "#FE6100")
blindsnake_tree <- phytools::paintSubTree(tree = blindsnake_tree, node = 71, state = "#FFB000")
blindsnake_tree <- phytools::paintSubTree(tree = blindsnake_tree, node = 82, state = "#938E71")
blindsnake_tree <- phytools::paintSubTree(tree = blindsnake_tree, node = 55, state = "#8E0CE3")

cols <- colnames(blindsnake_tree$mapped.edge)
names(cols) <- cols


plot(blindsnake_tree)

# pdf(file = "output/mcmctree_coloured.pdf", width = 13.33, height = 7.5)
# plot(blindsnake_tree, ftype= "i")
# axisPhylo()
# cladelabels(blindsnake_tree, 
            # node = c(84, 82, 75, 71, 60, 55), 
            # text = c("clade1", "clade2", "clade3", "clade4", "clade5", "clade6"),
            # wing.length = 0, offset = 0.5)

# dev.off()
# plot(blindsnake_tree, ftype= "off", lwd = 3)
# plot(blindsnake_tree, ftype= "i", lwd = 3)




# Shape tree --------------------------------------------------------------

# subset of taxa to avoid confusion
plot(shape_tree)

# COLOUR TIPS -------------------------------------------------------------

color_coder <- function(tree, number) {
  x <- getDescendants(tree, number)
  x <- x[which(x <= length(tree$tip.label))]
  
  return(x)
}

color.tips <- shape_tree$tip.label

plot(shape_tree)
nodelabels()

d1 <- color_coder(shape_tree, 65)
d2 <- color_coder(shape_tree, 63)
d3 <- color_coder(shape_tree, 57)
d4 <- color_coder(shape_tree, 56)
d5 <- color_coder(shape_tree, 48)
d6 <- color_coder(shape_tree, 46)
d7 <- color_coder(shape_tree, 41)

# specifiy colour for each node
color.tips[d1] <- "#648FFF"
color.tips[d2] <- "#FE6100"
color.tips[d3] <- "#FFB000"
color.tips[d4] <- "#938E71"
color.tips[d5] <- "#8E0CE3"
color.tips[d6] <- "#1FFA1A"
color.tips[d7] <- "#A5AED5"

'%notin%' <- Negate('%in%')

tip_colours_all <- c(color.tips, rep("black", shape_tree$Nnode))
# If tips not included then just grey
tip_colours_all[which(tip_colours_all %notin% c("#648FFF","#FE6100","#FFB000","#938E71","#8E0CE3","#1FFA1A","#A5AED5"))] <- 'grey'
names(tip_colours_all) <- 1:(length(shape_tree$tip.label) + shape_tree$Nnode)

# SHAPE DATA --------------------------------------------------------------

load('data/dorsal_head_shape.rda')

# FILTER DATA TO MATCH TREE TIPS -------------------------------------------

tree_tips_df <- as.data.frame(shape_tree$tip.label)
names(tree_tips_df) <- 'species_in_tree'

tree_tips_df$species_in_tree <- gsub('Anilios_', tree_tips_df$species, replacement = "", perl = TRUE)
# tree_tips_df$species_in_tree <- gsub('Ramphotyphlops_', tree_tips_df$species, replacement = "", perl = TRUE)
# tree_tips_df$species_in_tree <- gsub('Acutotyphlops_', tree_tips_df$species, replacement = "", perl = TRUE)
tree_tips_df$species_in_tree <- gsub('_', tree_tips_df$species, replacement = "", perl = TRUE)

# subset matrix to only contain species in tree
# rownames(Ypcs_matrix)[which(rownames(Ypcs_matrix) == "multillineatus")] <- "multilineatus"

ypc_subset <- Ypcs_matrix[tree_tips_df$species_in_tree, ]

# Add genus to rownames
rownames(ypc_subset) <- gsub(pattern = "^", replacement = "Anilios ", rownames(ypc_subset), perl = TRUE) 
# rownames(ypc_subset)[which(rownames(ypc_subset) == "Anilios subocularis")] <- "Acutotyphlops subocularis"
# rownames(ypc_subset)[which(rownames(ypc_subset) == "Anilios multilineatus")] <- "Ramphotyphlops multilineatus"

# prune tree again for only tips that we have data for
shape_data_tips <- gsub(x = as.vector(rownames(ypc_subset)), pattern = " ", replacement = "_") 

# PRUNED tree for shape
shape_phy <- ape::keep.tip(shape_tree, shape_data_tips)




# Paint the subset shape tree ---------------------------------------------

plot(shape_phy)
nodelabels()

col.pal <- c("#009E73", palette.colors(palette = "R4"))

# Paint tree
shape_tr_paint <- phytools::paintSubTree(tree = shape_phy, node = 65, state = col.pal[1])
shape_tr_paint <- phytools::paintSubTree(tree = shape_tr_paint, node = 63, state = col.pal[3])
shape_tr_paint <- phytools::paintSubTree(tree = shape_tr_paint, node = 57,state =  col.pal[4])
shape_tr_paint <- phytools::paintSubTree(tree = shape_tr_paint, node = 56, state = col.pal[5])
shape_tr_paint <- phytools::paintSubTree(tree = shape_tr_paint, node = 48, state = col.pal[6])
shape_tr_paint <- phytools::paintSubTree(tree = shape_tr_paint, node = 46, state = col.pal[7])
shape_tr_paint <- phytools::paintSubTree(tree = shape_tr_paint, node = 41, state = col.pal[8])


cols.pal <- colnames(shape_tr_paint$mapped.edge)
names(cols.pal) <- cols.pal


# dev.off()
# pdf(file = "output/shape_tree_coloured.pdf", width = 13.33, height = 7.5)

plotSimmap(shape_tr_paint, cols.pal ,pts=FALSE, ftype = 'i')
plot(shape_tr_paint)
axisPhylo()

# dev.off()



# SAVE SUBSET TREE --------------------------------------------------------

# save(shape_tr_paint, cols.pal, shape_phy, ypc_subset, sub_phy, tip_colours_all, file = 'data/subset_mcmctree_shape.rda')


