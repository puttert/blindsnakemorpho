# combine_climate_rasters.R
# Sarin Tiatragul

# Combine climate rasters and resample to match extent

# We downloaded raster files that contained modelled approximations at the resolution of 3 arc-seconds for soil bulk density 
# from the [CSIRO Soil and Landscape Grid of Australia](https://www.clw.csiro.au/aclep/soilandlandscapegrid/index.html) database, 
# climate data from WorldClim v.2 (https://www.worldclim.org/) [@fickWorldClim2017], and global aridity index from @zomerVersion2022. 
# We merged the three raster files by cropping the WorldClim and global aridity index rasters layers, then reprojecting and resampling 
# them to the same extent with the Australia-specific soil bulk density layer using the "project" and "resample" functions in 
# R package `terra` v.1.7-18 [@hijmansTerra2023]. From this merged raster, we extracted soil bulk density values between 0 — 5cm depth, 
# mean annual temperature, and aridity index for each georeferenced record using the "extract" function in 
# the R package `raster` v.3.6-3 [@hijmansRaster2022]. For each species we calculated the maximum bulk density, 
# mean annual temperature, and mean aridity index. Correlation analyses revealed that none of the variables were 
# strongly correlated with Pearson's *r* < 0.75. 


library(terra)

s1 <- rast("./data/worldclim2_30s/wc2.1_30s_bio_1.tif")
s2 <- rast("./data/slga_data/BDW_000_005_EV_N_P_AU_NAT_C_20140801.tif")

# Crop world layer to australia
s1_cropped <- crop(s1, terra::ext(s2))

# reproject s1 to the CRS of s2
s1_rprj <- project(s1_cropped, crs(s2))

# resample s2 to the resolution and extent of s1_rprj
s2_rsmp <- resample(s2, s1_rprj, method = "bilinear")

# stack the datasets
merged <- c(s1_rprj, s2_rsmp)

# Merge Bio13 = 
s3 <- rast("./data/global_aridity/Global-AI_ET0_v3_annual/ai_v3_yr.tif")

s3_rsp <- resample(s3, merged)

# Merge annual temp, soil, and driest month
merged <- c(merged, s3_rsp)

# save the merged raster as a tif file
writeRaster(merged, "./data/worldclim2_30s/merged_bio1_bdod_gai.tif", overwrite = TRUE)
