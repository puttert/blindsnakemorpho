# filename: 01_soil_slga_data.R
# September 2020 modified 2023-02-28
# Data from Soil and Landscape Grid of Australia (SLGA) database in GeoTif format. 

# The SLGA provides modelled soil attributes in different regions of Australia including: Bulk density, % sand, % clay, etc.

# Source script to get dataframe with all typhlopidae snake locality from museums in Australia.
# source('code/pre_00_specimens_with_rego_and_locality.R') then modified manually.

# libraries ---------------------------------------------------------------

library(raster); library(sp)
library(readr)
library(dplyr);library(tidyr)
library(ggplot2)

# Read in data
aus_blindsnake_locality <- read.csv(file = 'data/20230228_museum_combined_occurence.csv')

# Boundaries of SLGA data
# Max latitude: -9.9
# Min latitude: -43
# Max longitude: 153
# Min longitude: 112

aus_blindsnake_locality_df <- aus_blindsnake_locality %>% 
  filter(112.9 < longdec & longdec < 153.7) %>%  # restrict longitude to Australia excluding Tasmania
  filter(-43 < latdec & latdec < -9.9833) # restrict latitude to Australia excluding Tasmania

# import .tif data from SLGA
bulk_density_05 <- raster('data/slga_data/BDW_000_005_EV_N_P_AU_NAT_C_20140801.tif')
clay_density_05 <- raster('data/slga_data/CLY_000_005_EV_N_P_AU_NAT_C_20140801.tif')
silt_density_05 <- raster('data/slga_data/SLT_000_005_EV_N_P_AU_NAT_C_20140801.tif')
sand_density_05 <- raster('data/slga_data/SND_000_005_EV_N_P_AU_NAT_C_20140801.tif')

# Plot raster

plot(bulk_density_05)


cuts <- c(20, 50, 80, 100)
pal <- c("#1E88E5", "#FFC107", "#8F154E")
plot(sand_density_05, breaks = cuts, col = pal)


# Blind snake on the spots
points(aus_blindsnake_locality_df$longdec, aus_blindsnake_locality_df$latdec, pch=19, cex=.5, col = 2)

# Convert to SpatialPoints object
# specify the longitude and latitude in that order
# specify CRS proj4string: borrow CRS from bulk_density_05
# specify raster
distribution_spdf <- sp::SpatialPointsDataFrame(
  coords = aus_blindsnake_locality_df[,c('longdec','latdec')], 
  proj4string = bulk_density_05@crs,
  data = aus_blindsnake_locality_df)

distribution_spdf

## Extract data from circular buffer
# extract circular, 20m buffer
bulk_mean <- raster::extract(x = bulk_density_05,    # raster layer
                            y = distribution_spdf,   # SPDF with coordinates for buffer
                            fun = mean,               # what value to extract
                            df = TRUE)               # return a dataframe? 

clay_mean <- raster::extract(x = clay_density_05,    
                            y = distribution_spdf,   
                            fun = max,               
                            df = TRUE)              

silt_mean <- raster::extract(x = silt_density_05,    
                             y = distribution_spdf,   
                             fun = max,               
                             df = TRUE)      

sand_mean <- raster::extract(x = sand_density_05,    
                             y = distribution_spdf,   
                             fun = max,               
                             df = TRUE)   

# Append to data
aus_blindsnake_locality_df$pred_bulk_density_05 <- bulk_mean[,2]
aus_blindsnake_locality_df$pred_clay_mean_05 <- clay_mean[,2]
aus_blindsnake_locality_df$pred_silt_mean_05 <- silt_mean[,2]
aus_blindsnake_locality_df$pred_sand_mean_05 <- sand_mean[,2]

aus_blindsnake_locality_df


# Visualise  --------------------------------------------------------------

ggplot(data = aus_blindsnake_locality_df, aes(x = species, y = pred_bulk_density_05)) + 
  geom_boxplot()

ggplot(data = aus_blindsnake_locality_df, aes(y = pred_clay_mean_05, x = pred_bulk_density_05)) + 
  geom_point()

ggplot(data = aus_blindsnake_locality_df, aes(y = pred_silt_mean_05, x = pred_bulk_density_05)) + 
  geom_point()

ggplot(data = aus_blindsnake_locality_df, aes(y = pred_sand_mean_05, x = pred_bulk_density_05)) + 
  geom_point()


## From https://soilquality.org.au/factsheets/bulk-density-measurement
# 'The critical value of bulk density for restricting root growth varies with soil type (Hunt and Gilkes, 1992)
# but in general bulk densities greater than 1.6 g/cm3 tend to restrict root growth (McKenzie et al., 2004)'

### Summarise mean values
bulk_data_df <- aus_blindsnake_locality_df %>% 
  group_by(species) %>% 
  filter(!is.na(pred_bulk_density_05)) %>% 
  summarise(mean_bulk = mean(pred_bulk_density_05),
            mean_sand_per = mean(pred_sand_mean_05),
            mean_silt_per = mean(pred_silt_mean_05),
            mean_clay_per = mean(pred_clay_mean_05)) %>% 
  mutate(sum = mean_sand_per + mean_silt_per + mean_clay_per)

# Save as RDA

save(bulk_data_df, file = 'data/soil_bulk_density_data.rda')
