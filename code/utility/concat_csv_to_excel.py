# This script takes multiple csv files and concatenate them into an excel file with different tabs
# We can then use excel macro to format them to make a nice combination table
# Code is from stackoverflow https://stackoverflow.com/questions/51964001/merging-multiple-csv-files-into-separate-tabs-of-a-spreadsheet-in-python with some modifications using chatGPT

import os
import pandas as pd
import glob
import argparse

# Create an ArgumentParser object to parse the command-line arguments
# define the command-line arguments
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', type=str, help='input file pattern')
parser.add_argument('-o', '--output', type=str, help='output file name')
args = parser.parse_args()

# use the input argument to match the files
all_files = glob.glob(args.input)

writer = pd.ExcelWriter(args.output, engine='xlsxwriter')

for f in all_files:
    df = pd.read_csv(f)
    df.to_excel(writer, sheet_name=os.path.basename(f))

writer.close()

## USAGE
#  python C:\Users\ST\Documents\repo\blindsnakemorpho\code\utility\concat_csv_to_excel.py -i supp_contrasts_*.csv -o test.xlsx