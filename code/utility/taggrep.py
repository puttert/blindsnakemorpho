#!/usr/bin/env python3
try:
    import numpy as np
    from pyexiv2 import ImageMetadata
    from PIL import Image
    from tqdm import tqdm
except ImportError as exc:
    import sys
    print("Dependencies not met:", str(exc))
    print("Use one of the following to install dependencies:")
    print("\n\tpip install py3exiv2 numpy pillow tqdm msgpack pyyaml")
    print("\nor with conda:\n\n\tconda install exiv2 numpy pillow tqdm msgpack-python pyyaml && pip install py3exiv2")
    sys.exit(1)


from collections import defaultdict
from copy import deepcopy
from datetime import datetime as dt
import argparse
import io
from itertools import chain
import os
import os.path as op
import json
from pathlib import Path
import sys
from sys import stdin, stdout, stderr, exit
from shutil import copyfile
import traceback
import warnings


def mtime(path):
    """Get file modification time 
    """
    if not isinstance(path, Path):
        path = Path(path)
    return int(path.stat().st_mtime)


def find_files(bases, exts=None, skip_hidden=True, case_insensitive=False):
    if not isinstance(bases, list):
        bases = [bases, ]
    for base in bases:
        if not op.isdir(base):
            return base
        for root, dirs, files in os.walk(base, topdown=True):
            if root != "." and op.basename(root).startswith('.') and skip_hidden:
                dirs[:] = []
                continue
            for file in files:
                if file.startswith('.') and skip_hidden:
                    continue
                path = Path(root, file)
                pathsuf = path.suffix
                if case_insensitive:
                    pathsuf = pathsuf.lower()
                if exts is not None and pathsuf not in exts:
                    continue
                yield path

class SmarterImageMetadata(object):
    def __init__(self, filepath):
        self.main = ImageMetadata(filepath)
        self.main.read()
        if op.exists(filepath + ".xmp"):
            self.sidecar = ImageMetadata(filepath+".xmp")
            self.sidecar.read()
        else:
            self.sidecar = None

    def __getitem__(self, key):
        try:
            return self.main[key]
        except KeyError:
            if self.sidecar is not None:
                return self.sidecar[key]
            raise

    def __getattr__(self, attr):
        try:
            return getattr(self.main, attr)
        except AttributeError:
            if self.sidecar is not None:
                return getattr(self.sidecar, attr)
            raise

def get_file_metadata(filepath):
    try:
        im = SmarterImageMetadata(filepath)
        alltags = []
        try:
            alltags = list(im["Xmp.dc.subject"].value)
        except KeyError:
            pass
        tags = [t for t in alltags if not t.startswith("_")]
        taxa = [op.basename(t) for t in tags if t.startswith("TAXON")]
        try:
            title = "\n".join(im["Iptc.Application2.ObjectName"].value)
        except KeyError:
            title = ""
        try:
            caption = "\n".join(im["Iptc.Application2.Caption"].value)
        except KeyError:
            caption = ""
        try:
            datetime = im['Exif.Photo.DateTimeOriginal'].value.isoformat(sep=' ')
        except (KeyError, AttributeError) as exc:
            datetime = None
        return {
            "mtime": mtime(filepath),
            "private": "_PRIVATE" in alltags,
            "capture_datetime": datetime,
            "tags": tags,
            "taxa": taxa,
            "title": title,
            "caption": caption,
            "filepath": filepath,
            "height": im.dimensions[1],
            "width": im.dimensions[0],
        }
    except Exception as exc:
        print("Failure in", filepath)
        traceback.print_traceback()


def tag_search(needle, tags):
    return any([needle.lower() == x.lower() for x in tags])

def main():
    ap = argparse.ArgumentParser()
    ap.add_argument("--extension", "-e", action="append", default=[".jpg", ], dest="extensions",
            help="File extensions to process (default jpg)"),
    ap.add_argument("--tsv-out", "-o",  required=True, type=argparse.FileType("w"),
            help="Output TSV for images that match query.")
    ap.add_argument("--search", "-s",  required=True, type=str,
            help="Search string. Matches case insensitively.")
    ap.add_argument("basedir", default=".", nargs='+')
    args = ap.parse_args()

    for file in tqdm(find_files(args.basedir, exts=args.extensions, case_insensitive=True), desc="Crawling the freakin imgs"):
        file = str(file)
        meta = get_file_metadata(file)
        if tag_search(args.search, meta["tags"]):
            print(file, "; ".join(meta["tags"]), sep="\t", file=args.tsv_out)

if __name__ == "__main__":
    main()
