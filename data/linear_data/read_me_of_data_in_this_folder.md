# Read me for data in /linear_data/

## linear_data_measurements_df.csv 
2021-09-08

This is a dataframe written from read_only data from multiple raw linear measurements. 

## body_shape_ratio.csv
2022-01-18

This dataframe includes body proportions (sensu Mosimann 1970). This is achieved by dividing all variables with body size (svl). The columns with body proportions have 'sh_' prefix. 

NOTE: As of 2022-01-18 This body_shape_ratio data set includes imputated values using the 'predictive mean matching' method from the `mice` package. Initially this is done to impute missing skull length data because QM X-ray are not yet available. 


### Script
The script used to compile and process these these data frames is "../code/linear_measurements_data.R"