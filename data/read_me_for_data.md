# Read me for /data

cytb_trees.rda - combination of full phylogeny, subset tree based on what is in the data, and tip colours. Generated from code `phylogeny_cytb_subset.R`

dorsal_head_shape.rda - symmetric components of dorsal shape and means per species. Generated from code `tps_landmark_to_geomorph_data.R`

MCC_10_meanh.tre - generated from BEAST. Data based on cytb only.

soil_bulk_density_data.rda - Extracted bulk soil density data from CSIRO Soil and Landscape Grid of Australia [DOI](https://doi.org/10.4225/08/546EE212B0048). This is a data frame of mean bulk density per species based on soil data from each occurence. Data generated from `soil_00_slga_data.R` 

20220202_museum_combined_occurrence.csv - Occurence record compiled from independent museum specimen database. Code `pre_00_specimens_with_rego_and_locality.R`